'use strict';

const gulp = require('gulp');
const changed = require('gulp-changed');
const browserSync = require('browser-sync').create();
const pug = require('gulp-pug');
const compass = require('gulp-compass');
const concat = require('gulp-concat');
const htmllint = require('gulp-htmllint');
const fancyLog = require('fancy-log');
const colors = require('ansi-colors');
const csslint = require('gulp-csslint');
const jshint = require('gulp-jshint');
const stylish = require('jshint-stylish');

const conf = {
  'htmlSrc': "./pug/**/*.pug",
  'scssSrc': "./scss/**/*.scss",
  'project' : 'fc-gulp',
  'src'  : '',
  'dest' : '../'
};

function handleError(err) {
  console.log(err.toString());
  this.emit('end');
}

// Static server
gulp.task('browser-sync', function() {
  browserSync.init({
    files: [conf.dest+'**/*.{html,css,js}', !conf.src+'**/*.*'],
    port : 3000,
    server: {
      baseDir: conf.dest
    }
  });
});

// HTML
gulp.task('pug', function() {
  gulp.src(['./pug/**/*.pug', '!./pug/**/_*.pug'])
  //.pipe(changed(conf.htmlSrc, {hasChanged: changed.compareContent}))
  .pipe(pug({
    pretty: true
  }).on('error', handleError))
  .pipe(gulp.dest(conf.dest))
  .pipe(browserSync.stream());
});

gulp.task('compass', function() {
  gulp.src(conf.scssSrc)
    .pipe(changed('../css'))
    .pipe(compass({
      css: conf.dest+'/css',
      sass: 'scss',
      sourcemap: true
    }).on('error', handleError))
    .pipe(gulp.dest(conf.dest+'/css'))
    .pipe(browserSync.stream());
});

gulp.task('htmllint', function() {
  return gulp.src(['../**/*.html', '!../dev/**'])
    .pipe(htmllint({htmlhintrc: './.htmlhintrc'}, htmllintReporter));
});

function htmllintReporter(filepath, issues) {
  if (issues.length > 0) {
    issues.forEach(function (issue) {
      fancyLog(colors.cyan('[gulp-htmllint] ') + colors.white(filepath + ' [' + issue.line + ',' + issue.column + ']: ') + colors.red('(' + issue.code + ') ' + issue.msg));
    });

    process.exitCode = 1;
  }
}

gulp.task('csslint', function() {
  gulp.src(conf.dest+'css/**/*.css')
    .pipe(csslint({
      'force': true,
      'import': 1,
      'vendor-prefix': 2,
      'order-alphabetical': false,
      'compatible-vendor-prefixes': false,
      'known-properties': false,
      'selector-newline': false,
      'box-model': false,
      'adjoining-classes': false,
      'important': false,
      'box-sizing': false,
      'gradients': false,
      'universal-selector': false,
      'star-property-hack': false,
      'fallback-colors': false,
      'overqualified-elements': false,
      'duplicate-background-images': false,
      'text-indent': false,
      'floats': false,
      'outline-none': false,
      'font-sizes': false,
    }))
    .pipe(csslint.formatter());
});

gulp.task('jshint', function() {
  return gulp.src(conf.dest+'js/scripts.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'));
});

// Watch
gulp.task('watch', function () {
  gulp.watch(conf.htmlSrc, ['pug']);
  gulp.watch(conf.scssSrc, ['compass']);
});

// Default
gulp.task('default', ['watch', 'browser-sync', 'htmllint', 'csslint', 'jshint']);

gulp.task('quick', ['watch', 'browser-sync']);

gulp.task('validate', ['htmllint', 'csslint', 'jshint']);
