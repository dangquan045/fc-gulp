# Gulpbase

Sample gulpfile.js for HTML template (default: PUG) and CSS preprocessors (compass).


## Package avaiable
- [gulp](https://www.npmjs.com/package/gulp)
- [gulp-changed](https://www.npmjs.com/package/gulp-changed)
- [gulp-pug](https://www.npmjs.com/package/gulp-pug)
- [gulp-compass](https://www.npmjs.com/package/gulp-compass)
- [browser-sync](https://www.npmjs.com/package/browser-sync)


## Usage

Install gulpjs and some dependencies.

  > $ npm install

Run gulp.

  > $ gulp


# Features

* Active page current in navigation menu (PUG)
* Option include javascript / stylesheet at header or footer (PUG)
* Browsers are automatically updated as you change HTML, CSS. (GULP)
* Allow loop array, expression condition, functions,... (PUG)